﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public class Rocinante:Vaisseau
    {
        public Rocinante()
        {
            this.pointsDeBouclierMax = 5;
            this.pointsDeStructureMax = 3;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
            this.armesEmbarquees.Add(new Arme("Torpille", 3, 3, EType.Guidé, 2));
        }

        public override string ToString()
        {
            return "Rocinante";
        }

        public override void Attaque(Vaisseau vaisseauCible)
        {
            foreach(Arme arme in this.armesEmbarquees)
            {
                int dommages = arme.SimulationDeTir();
                vaisseauCible.RepartionDommages(dommages);
                if (dommages > 0)
                {
                    arme.ResetCompteurTours();
                    Console.WriteLine("Le vaisseau vous inflige " + dommages + " dommages !");
                }
                else
                {
                    Console.WriteLine("Mais le vaisseau vous rate !");
                }
            }

            
        }
    }
}
