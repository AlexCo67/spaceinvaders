﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace TP1_CLASSES
{
    public class Joueur
    {
        public Joueur(string nom, string prenom, string pseudo) {
            this.nom = FormaterStringMajusculePuisMinuscules(nom);
            this.prenom = FormaterStringMajusculePuisMinuscules(prenom);
            this.pseudo = pseudo;
            this.vaisseau = new ViperMKII();
        }

        private string nom;
        private string prenom;
        private string pseudo;
        private Vaisseau vaisseau;

        //GETTERS
        public string GetPseudo()
        {
            return this.pseudo;
        }
        public string GetNom()
        {
            return this.nom;
        }
        public string GetPrenom()
        {
            return this.prenom;
        }
        public Vaisseau GetVaisseau()
        {
            return this.vaisseau;
        }

        //TOSTRING
        public override string ToString()
        {
            return pseudo+"("+prenom+" "+nom+")";
        }

        //CONCATENATION DU NOM ET PRENOM
        public string ConcatenationNomPrenom()
        {
            return this.nom + this.prenom;
        }

        //EQUALS
        public override bool Equals(object obj)
        {
            var joueur = obj as Joueur;
            if (joueur == null)
            {
                return false;
            }
            else
            {
                return this.GetPseudo().Equals(joueur.GetPseudo());
            }
        }

        //FORMATAGE DES NOMS
        private static string FormaterStringMajusculePuisMinuscules(string nom)
        {
            string result = string.Empty;
            result = char.ToUpper(nom[0]) + nom.Substring(1).ToLower();
            return result;
        }

        //Afficher les données d'un joueur (données joueur + vaisseau + armes)
        public void AfficherInfosJoueur() 
        {
            Console.WriteLine(this.ToString());
            Console.WriteLine(this.GetVaisseau().ToString());
            this.GetVaisseau().AfficherListeArmes();
        }

    }
}
