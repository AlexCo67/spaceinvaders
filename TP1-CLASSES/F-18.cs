﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public class F_18 : Vaisseau, IAptitude
    {

        public F_18()
        {
            this.pointsDeBouclierMax = 0;
            this.pointsDeStructureMax = 15;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
        }
        public override void Attaque(Vaisseau vaisseauCible)
        {
                Console.WriteLine("F-18 s'approche de façon menaçante !");
        }

        public override string ToString()
        {
            return "F-18";
        }
        public void Utilise(List<Vaisseau> listeVaisseau, Vaisseau vaisseauCible)
        {
            if (listeVaisseau[0].Equals(this))
            {
                vaisseauCible.RepartionDommages(10);

                this.pointsDeStructure = 0;
                this.detruit = true;
                Console.WriteLine("Le F-18 s'autodétruit, votre vaisseau encaisse 10 dommages !");
            }
        }
    }
}
