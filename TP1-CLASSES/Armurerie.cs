﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
   public class Armurerie
    {
        List<Arme> listeArmes = new List<Arme>();

        public Armurerie()
        {
            Init();
        }

        private void Init()
        {
            listeArmes.Add(new Arme("LaserOP", 200, 100, EType.Direct));
            listeArmes.Add(new Arme("NukeOP", 300, 200, EType.Explosif));
            listeArmes.Add(new Arme("MissileOP", 100, 50, EType.Guidé));
            listeArmes.Add(new Arme("Laser", 3, 2, EType.Direct,1));
            listeArmes.Add(new Arme("Hammer", 8, 1, EType.Explosif, 1.5));
            listeArmes.Add(new Arme("Torpille", 3, 3, EType.Guidé, 2));
            listeArmes.Add(new Arme("Mitrailleuse", 3, 2, EType.Direct, 1));
            listeArmes.Add(new Arme("EMG", 7, 1, EType.Explosif, 1.5));
            listeArmes.Add(new Arme("Missile", 100, 4, EType.Guidé, 4));
        }
        //GETTERS

        public List<Arme> GetListeArmes()
        {
            return this.listeArmes;
        }

        //AFFICHER

        public void Afficher()
        {
            Console.WriteLine("Informations sur l'armurerie : ");
            foreach(Arme arme in this.GetListeArmes())
            {
                Console.WriteLine(arme.ToString());
            }
            return;
        }

    }

}
