﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public interface IAptitude
    {
        public void Utilise(List<Vaisseau> listeVaisseau, Vaisseau vaisseauCible);
    }
}
