﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TP1_CLASSES
{
    public class Tardis:Vaisseau, IAptitude
    {
        public Tardis()
        {
            this.pointsDeBouclierMax = 0;
            this.pointsDeStructureMax = 1;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
        }

        public override void Attaque(Vaisseau vaisseauCible)
        {
            Console.WriteLine("Le Tardis tourne en rond dans l'espace.");
        }

        public override string ToString()
        {
            return "Tardis";
        }

        public void Utilise(List<Vaisseau> listeVaisseau, Vaisseau vaisseauCible)
        {
            Random rdn = new Random();
            int indexvaiseau1 = rdn.Next(0, listeVaisseau.Count - 1);
            int indexvaiseau2 = rdn.Next(0, listeVaisseau.Count - 1);
            Console.WriteLine("Le tardis téléporte " + listeVaisseau[indexvaiseau1].ToString() + " et " + listeVaisseau[indexvaiseau2]+" ! ");
            Vaisseau tmp = listeVaisseau[indexvaiseau1];
            listeVaisseau[indexvaiseau1] = listeVaisseau[indexvaiseau2];
            listeVaisseau[indexvaiseau2] = tmp;          
        }
    }

}
