﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace TP1_CLASSES
{
    public abstract class Vaisseau
    {
        protected int pointsDeStructureMax;
        protected int pointsDeBouclierMax;
        protected int pointsDeStructure;
        protected int pointsDeBouclier;
        protected bool detruit;
        protected List<Arme> armesEmbarquees = new List<Arme>();


        public Vaisseau()
        {
            this.pointsDeStructureMax = 100;
            this.pointsDeBouclierMax = 200;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
            this.detruit = false;
            //armesEmbarquees.Add(new Arme("Laser", 200, 100, EType.Direct));
            //armesEmbarquees.Add(new Arme("Nuke", 300, 200, EType.Explosif));
            //armesEmbarquees.Add(new Arme("Missile", 100, 50, EType.Guidé));
        }

        //GETTERS
        public int GetPointsDeStructureMax()
        {
            return this.pointsDeStructureMax;
        }
        public int GetPointsDeBouclierMax()
        {
            return this.pointsDeBouclierMax;
        }
        public bool GetDetruit()
        {
            return this.detruit;
        }
        public List<Arme> GetArmesEmbarquees()
        {
            return this.armesEmbarquees;
        }
        public int GetPointsDeBouclier()
        {
            return this.pointsDeBouclier;
        }

        public int GetPointsDeStructure()
        {
            return this.pointsDeStructure;
        }

        public void Destruction()
        {
            if (this.pointsDeStructure <= 0)
            {
                this.detruit = true;
            }
        }

        public void RegenerationDeBouclier()
        {
            if (this.pointsDeBouclier <= this.pointsDeBouclierMax - 2)
            {
                this.pointsDeBouclier = this.pointsDeBouclier + 2;
            }
            else if (this.pointsDeBouclier == this.pointsDeBouclierMax - 1)
            {
                this.pointsDeBouclier=this.pointsDeBouclierMax;
            }
        }

        //GESTION DES ARMES
        public void AjouterArme(Arme arme)
        {
            //TRY CATCH : TEST DE L'ARME DANS L'ARMURERIE ET TEST DU NOMBRE D'ARME < 3
            try
            {
                Armurerie armurerie = new Armurerie();
                if (!armurerie.GetListeArmes().Contains(arme))
                {
                    throw new ArmurerieException("Weapon not in armory list");
                }

                if (armesEmbarquees.Count >= 3)
                {
                    throw new Exception("Too many weapons on the spaceship");
                }
                armesEmbarquees.Add(arme);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error when adding weapon : "+e.Message);
            }
       }

        public void RetirerArme(Arme arme)
        {
            try
            {
                armesEmbarquees.Remove(arme);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when removing weapon" + e.Message);
            }

        }

        //AFFICHAGE D'INFORMATIONS
        public void AfficherListeArmes()
        {
            Console.WriteLine("Voici la liste des armes embarquées sur le vaisseau :");
            foreach (Arme arme in this.GetArmesEmbarquees())
            {
                
                Console.WriteLine(arme.ToString());
            }
            return;
        }

        public int DegatsMoyensVaisseau()
        {
            int result=0;
            int nbArmes = 0;
            foreach(Arme arme in this.GetArmesEmbarquees())
            {
                result=result+arme.MoyenneDommagesArme();
                nbArmes = nbArmes + 1;
            }
            return result/nbArmes;
        }

        public void RepartionDommages(int dommages)
        {
            int dommagesRestants = 0;
            if (this.pointsDeBouclier > 0)
            {
                this.pointsDeBouclier = this.pointsDeBouclier - dommages;
                if (this.pointsDeBouclier < 0)
                {
                    dommagesRestants = -this.pointsDeBouclier;
                    this.pointsDeBouclier = 0;
                }
            }
            else
            {
                dommagesRestants = dommages;
            }
            if (this.pointsDeStructure > 0)
            {
                this.pointsDeStructure = this.pointsDeStructure - dommagesRestants;
            }
            this.Destruction();
        }

        public abstract void Attaque(Vaisseau vaisseauCible);

        public void AfficherVie()
        {
            Console.WriteLine("HP : " + this.pointsDeStructure + ", Shield : " + this.pointsDeBouclier);
        }


        //TOSTRING (ne renvoie pas la liste d'armes dans la string, utiliser AfficherListeArmes pour le détail)

        public override string ToString()
        {
            return ("HP max : "+this.GetPointsDeStructureMax()+", shield max : "+this.GetPointsDeBouclierMax()+", détruit :"+this.GetDetruit()+", dégats moyens : "+this.DegatsMoyensVaisseau());
        }
    }
}
