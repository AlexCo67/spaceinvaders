﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    [Serializable]
    public class ArmurerieException : Exception
    {

        public ArmurerieException()
        {

        }
        public ArmurerieException(string message) : base(message)
        { 
        
        }

        public ArmurerieException(string message, Exception innerException) : base(message, innerException)
        { 
        
        }
    }
}
