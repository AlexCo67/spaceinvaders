﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public class Dart : Vaisseau
    {
        public Dart()
        {
            this.pointsDeStructureMax = 10;
            this.pointsDeBouclierMax = 3;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
            this.armesEmbarquees.Add(new Arme("Laser", 3, 2, EType.Direct, 1));
        }

        public override string ToString()
        {
            return "Dart";
        }

        public override void Attaque(Vaisseau vaisseauCible)
        {
            foreach (Arme arme in this.armesEmbarquees)
            {
                int dommages = arme.SimulationDeTir();
                vaisseauCible.RepartionDommages(dommages);
                if (dommages > 0)
                {
                    arme.ResetCompteurTours();
                    Console.WriteLine("Le vaisseau vous inflige " + dommages + " dommages !");
                }
                else
                {
                    Console.WriteLine("Mais le vaisseau vous rate !");
                }
            }
        }
    }
}
