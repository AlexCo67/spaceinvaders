﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public class ViperMKII:Vaisseau
    {
        public ViperMKII()
        {
            this.pointsDeBouclierMax = 50;
            this.pointsDeStructureMax = 50;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
            this.armesEmbarquees.Add(new Arme("Mitrailleuse", 3, 2, EType.Direct, 1));
            this.armesEmbarquees.Add(new Arme("EMG", 7, 1, EType.Explosif, 1.5));
            this.armesEmbarquees.Add(new Arme("Missile", 100, 4, EType.Guidé, 4));
        }
        public override void Attaque(Vaisseau vaisseauCible)
        {
            int dommages;
            int dommagesMissile = this.armesEmbarquees[2].SimulationDeTir();
            int dommagesEMG = this.armesEmbarquees[1].SimulationDeTir();
            int dommagesMitraillette = this.armesEmbarquees[0].SimulationDeTir();
            if (dommagesMissile > 0)
            {
                Console.WriteLine("Vous tirez aux missiles");
                this.armesEmbarquees[2].ResetCompteurTours();
                dommages = dommagesMissile;
            }
            else if (dommagesEMG > 0)
            {
                Console.WriteLine("Vous tirez à l'EMG");
                this.armesEmbarquees[1].ResetCompteurTours();
                dommages = dommagesEMG;
            }
            else if(dommagesMitraillette>0)
            {
                Console.WriteLine("Vous tirez à la mitrailleuse");
                this.armesEmbarquees[0].ResetCompteurTours();
                dommages = dommagesMitraillette;
            }
            else
            {
                dommages = 0;
            }
            if(dommages>0)
            {
                Console.WriteLine("Vous infligez " + dommages + " dommages !");
                vaisseauCible.RepartionDommages(dommages);
            }
            else
            {
                Console.WriteLine("Vous ratez votre attaque !");
            }
            
        }
    }
}
