﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP1_CLASSES
{
    public class B_Wings:Vaisseau
    {
        public B_Wings()
        {
            this.pointsDeStructureMax = 30;
            this.pointsDeBouclierMax = 0;
            this.pointsDeStructure = this.pointsDeStructureMax;
            this.pointsDeBouclier = this.pointsDeBouclierMax;
            this.armesEmbarquees.Add(new Arme("Hammer", 8, 1, EType.Explosif, 1.5));
        }

        public override string ToString()
        {
            return "B-Wings";
        }
        public override void Attaque(Vaisseau vaisseauCible)
        {
            foreach (Arme arme in this.armesEmbarquees)
            {
                int dommages = arme.SimulationDeTir();
                vaisseauCible.RepartionDommages(dommages);
                if (dommages > 0)
                {
                    arme.ResetCompteurTours();
                    Console.WriteLine("Le vaisseau vous inflige " + dommages + " dommages !");
                }
                else
                {
                    Console.WriteLine("Mais le vaisseau vous rate !");
                }
            }


        }
    }
}
