﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace TP1_CLASSES
{
    class SpaceInvaders
    {
        private List<Joueur> listeJoueurs=new List<Joueur>();
        private List<Vaisseau> listeVaisseaux = new List<Vaisseau>(); 

        SpaceInvaders() { 
        
            Init();
        }
        //On initialise une liste de 3 joueurs.
        private void Init(){ 
            Joueur joueur1 = new Joueur("Jean", "Michel", "DarkSasuke69");
            Joueur joueur2 = new Joueur("Joseph", "Stalin", "IRONBEAR");
            Joueur joueur3 = new Joueur("test", "mctest", "TrainingDummy");
            listeJoueurs.Add(joueur1);
            listeJoueurs.Add(joueur2);
            listeJoueurs.Add(joueur3);
            Tardis tardis = new Tardis();
            listeVaisseaux.Add(tardis);
            Dart dart = new Dart();
            listeVaisseaux.Add(dart);
            F_18 f_18 = new F_18();
            listeVaisseaux.Add(f_18);
            Rocinante rocinante = new Rocinante();
            listeVaisseaux.Add(rocinante);
            B_Wings b_wings = new B_Wings();
            listeVaisseaux.Add(b_wings);
        }

        //GETTERS
        public List<Joueur> GetListeJoueurs()
        {
            return this.listeJoueurs;
        }

        public List<Vaisseau> GetListeVaisseaux()
        {
            return this.listeVaisseaux;
        }

        //AFFICHAGE DES INFOS DE TOUS LES JOUEURS
        public void AfficherInfosTousJoueurs()
        {
            Console.WriteLine("Informations sur les joueurs : ");
            foreach(Joueur joueur in this.GetListeJoueurs())
            {
                joueur.AfficherInfosJoueur();
            }
            return;
        }

        public void RegenerationBoucliersDesVaisseaux()
        {
            foreach(Vaisseau vaisseau in this.listeVaisseaux)
            {
                vaisseau.RegenerationDeBouclier();
            }
        }

        public void Aptitude(Vaisseau vaisseauJoueur)
        {
            bool tardisTeleport = false;
            for (int i = 0; i < this.listeVaisseaux.Count; ++i)
            {
                if (this.listeVaisseaux[i] is IAptitude)
                {
                    if (this.listeVaisseaux[i].GetType().Equals(typeof(Tardis)) && !tardisTeleport)
                    {
                        tardisTeleport = true;
                        ((IAptitude)this.listeVaisseaux[i]).Utilise(this.listeVaisseaux, vaisseauJoueur);
                    }
                    else if (!this.listeVaisseaux[i].GetType().Equals(typeof(Tardis)))
                    {
                        ((IAptitude)this.listeVaisseaux[i]).Utilise(this.listeVaisseaux, vaisseauJoueur);
                    }
                    
                }
            }
        }


        public void Tour(Vaisseau vaisseauJoueur)
        {
            Random rdn = new Random();
            this.RegenerationBoucliersDesVaisseaux();
            vaisseauJoueur.RegenerationDeBouclier();
            this.Aptitude(vaisseauJoueur);
            bool joueurAttaque = false;
            int nombreVaisseauxJeu=0;
            foreach (Vaisseau vaisseau in this.listeVaisseaux)
            {
                if (!joueurAttaque && (rdn.Next(1,this.listeVaisseaux.Count-nombreVaisseauxJeu)==1) &&!vaisseauJoueur.GetDetruit())
                {
                    joueurAttaque = true;
                    int indexVaisseauCible = rdn.Next(0, this.listeVaisseaux.Count-1);
                    Console.WriteLine("Vous attaquez le vaisseau " + listeVaisseaux[indexVaisseauCible].ToString());
                    vaisseauJoueur.Attaque(this.listeVaisseaux[indexVaisseauCible]);
                    Console.WriteLine("Statut du vaisseau cible : ");
                    this.listeVaisseaux[indexVaisseauCible].AfficherVie();
                    if (this.listeVaisseaux[indexVaisseauCible].GetDetruit())
                    {
                        Console.WriteLine("VAISSEAU ENNEMI DETRUIT !");
                    }
                }
                
                if (!(this.listeVaisseaux.Count==0)&&!vaisseau.GetDetruit())
                {
                    Console.WriteLine(vaisseau.ToString()+ " vous attaque");
                    vaisseau.Attaque(vaisseauJoueur);
                    Console.WriteLine("Statut du vaisseau joueur : ");
                    vaisseauJoueur.AfficherVie();
                    vaisseauJoueur.Destruction();
                    if (vaisseauJoueur.GetDetruit() == true)
                    {
                        
                        Console.WriteLine("Votre vaisseau explose sous les tirs ennemis ! L'explosion fracture le cockpit, détruisant au passage les systèmes de survie de votre combinaison spatiale.");
                        Console.WriteLine("En quelques instants, le vide intersidéral aspire votre carcasse mutilée. Vous flottez parmi les débris de vaisseau carbonisés, dérivant lentement au milieu du silence et de l'obscurité.");
                    }

                }
                nombreVaisseauxJeu = nombreVaisseauxJeu + 1;
            }
            Console.WriteLine("");

            for (int i = 0; i < this.listeVaisseaux.Count; ++i)
            {
                if (this.listeVaisseaux[i].GetDetruit())
                {
                    this.listeVaisseaux.Remove(this.listeVaisseaux[i]);
                }
            }
        }

        public void AfficherVaisseauxEnnemisEncoreEnVie()
        {
            Console.WriteLine("Vaisseaux ennemis restants : ");
            foreach (Vaisseau vaisseau in this.listeVaisseaux)
            {
                Console.WriteLine(vaisseau.ToString());
                vaisseau.AfficherVie();
            }
        }

        static void Main(string[] args) 
        {
            SpaceInvaders spaceInvaders = new SpaceInvaders();
            Armurerie armurerie = new Armurerie();
            //    Console.WriteLine("Liste des joueurs :");
            //    foreach(Joueur joueur in spaceInvaders.listeJoueurs)
            //        {
            //            Console.WriteLine(joueur.ToString());
            //        }
            //    Console.WriteLine("");
            //    armurerie.Afficher();
            //    Console.WriteLine("");
            //    spaceInvaders.AfficherInfosTousJoueurs();
            //    Console.WriteLine("");
            //    Console.WriteLine("test de retrait d'arme");
            //    //Ligne de test à changer absolument en créant une fonction de getters pour obtenir : le vaisseau d'un joueur, une arme embarquée dans un vaisseau.)
            //    spaceInvaders.GetListeJoueurs()[0].GetVaisseau().RetirerArme(spaceInvaders.GetListeJoueurs()[0].GetVaisseau().GetArmesEmbarquees()[0]);
            //    spaceInvaders.GetListeJoueurs()[0].AfficherInfosJoueur();
            //    Console.WriteLine("");
            //    Console.WriteLine("test d'ajout d'arme");
            //    spaceInvaders.GetListeJoueurs()[0].GetVaisseau().AjouterArme(armurerie.GetListeArmes()[0]);
            //    spaceInvaders.GetListeJoueurs()[0].AfficherInfosJoueur();
            //    Console.WriteLine("");
            //    Console.WriteLine("test d'ajout d'arme en trop, provoquant une erreur");
            //    spaceInvaders.GetListeJoueurs()[0].GetVaisseau().AjouterArme(armurerie.GetListeArmes()[0]);
            //    Console.WriteLine("");
            //    Console.WriteLine("test d'ajout d'arme n'appartenant pas à l'armurerie, provoquant une erreur");
            //    Arme armeBidon = new Arme("arme bidon", 12, 55, EType.Explosif);
            //    spaceInvaders.GetListeJoueurs()[0].GetVaisseau().AjouterArme(armeBidon);
            //    Console.WriteLine("");
            //    Console.WriteLine("tout se passe bien");

            int tourDeJeu = 1;
            while (!spaceInvaders.GetListeJoueurs()[0].GetVaisseau().GetDetruit() && !(spaceInvaders.GetListeVaisseaux().Count == 0))
            {
                Console.WriteLine("");
                Console.WriteLine("=================");
                Console.WriteLine("");
                Console.WriteLine("Tour : " + tourDeJeu);
                spaceInvaders.Tour(spaceInvaders.GetListeJoueurs()[0].GetVaisseau());
                Console.WriteLine("Statut du vaisseau joueur : ");
                spaceInvaders.GetListeJoueurs()[0].GetVaisseau().AfficherVie();
                spaceInvaders.AfficherVaisseauxEnnemisEncoreEnVie();
                tourDeJeu = tourDeJeu + 1;
            }

            Console.WriteLine("");
            if (spaceInvaders.GetListeJoueurs()[0].GetVaisseau().GetDetruit())
            {
                Console.WriteLine("Game Over");
                Console.WriteLine("Quelle surprise ! En même temps les statistiques des vaisseaux ennemis sont éclatées. Ou bien j'ai mal compris des choses dans le TP.");
            }
            else if(spaceInvaders.GetListeVaisseaux().Count == 0){
                Console.WriteLine("Vous êtes forts en RNG !!!");
            }
        }
    }
}
