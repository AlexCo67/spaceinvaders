﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace TP1_CLASSES
{
   
    public class Arme
    {
        private string nom;
        private int damageMax;
        private int damageMin;
        private EType typeArme;
        private double tempsDeRechargement;
        private double compteurDeTours;

        public Arme(string nom, int damageMax, int damageMin, EType typeArme)
        {
            this.nom = nom;
            this.damageMax = damageMax;
            this.damageMin = damageMin;
            this.typeArme = typeArme;
            this.tempsDeRechargement= 1;
            this.compteurDeTours = this.tempsDeRechargement;
        }

        public Arme(string nom, int damageMax, int damageMin, EType typeArme, double tempsReload)
        {
            this.nom = nom;
            this.damageMax = damageMax;
            this.damageMin = damageMin;
            this.typeArme = typeArme;
            this.tempsDeRechargement = tempsReload;
            this.compteurDeTours = this.tempsDeRechargement;
        }

        //GETTERS
        public string GetNom()
        {
            return this.nom;
        }

        public EType GetTypeArme()
        {
            return this.typeArme;
        }

        public int GetDamageMin()
        {
            return this.damageMin;
        }

        public int GetDamageMax()
        {
            return this.damageMax;
        }

        public double GetCompteurDeTour()
        {
            return this.compteurDeTours;
        }

        public double GetTempsRechargement()
        {
            return this.tempsDeRechargement;
        }

        //RESET COMPTEUR TOUR

        public void ResetCompteurTours()
        {
            this.compteurDeTours = this.tempsDeRechargement;
        }
        //MOYENNE DOMMAGES DE L'ARME

        public int MoyenneDommagesArme()
        {
            return (this.GetDamageMax() + this.GetDamageMin()) / 2;
        }

        //SIMULATION DE TIR

        public int SimulationDeTir()
        {
            Random rnd = new Random();
            this.compteurDeTours = this.compteurDeTours - 1;
            int dommages = rnd.Next(this.damageMin, this.damageMax);
            if (this.compteurDeTours <= 0)
            {
                if (this.typeArme.Equals(EType.Direct))
                {
                    int chanceToucher = rnd.Next(1, 10);
                    if (chanceToucher == 10)
                    {
                        return 0;
                    }
                    else
                    {
                        return (dommages);
                    }
                }
                else if (this.typeArme.Equals(EType.Explosif))
                {
                    this.compteurDeTours = this.compteurDeTours * 2;
                    dommages = dommages * 2;
                    int chanceToucher = rnd.Next(1, 4);
                    if (chanceToucher == 4)
                    {
                        return 0;
                    }
                    else
                    {
                        return (dommages);
                    }
                }
                else if (this.typeArme.Equals(EType.Guidé))
                {
                    return this.damageMin;
                }
            }
            return 0;
        }

        //EQUALS

        public override bool Equals(object obj)
        {
            var arme = obj as Arme;
            if (arme == null)
            {
                return false;
            }
            else
            {
                return this.GetNom().Equals(arme.GetNom());
            }
        }

        //TOSTRING

        public override string ToString()
        {
            string result = string.Empty;
            result = "Arme : " + nom + ", damage range :" + damageMin + " - " + damageMax + ", damage type : " + typeArme;
            return result;
        }
    }
}
